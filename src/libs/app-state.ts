import { constants, APP_STATE } from "./constants";

const { APP_STATES_KEY } = constants;

type State = {
  [K in keyof typeof APP_STATES_KEY]: APP_STATE[K];
};

class AppState {
  private static instance: AppState;
  private state: State;

  private constructor() {
    this.state = {
      INIT: false,
      ROUTER_EXEC: 0,
    };

    console.info("TT");
  }

  public static getInstance(): AppState {
    if (!AppState.instance) {
      AppState.instance = new AppState();
    }
    return AppState.instance;
  }

  public setState(key: string, value: any): void {
    this.state[key] = value;
  }

  public getState(key: string): any {
    return this.state[key];
  }
}

// Uso del State
export const appState = AppState.getInstance();
