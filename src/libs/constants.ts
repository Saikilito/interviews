export type APP_STATE = {
  INIT: boolean;
  ROUTER_EXEC: number;
};

export const constants = {
  APP_STATES_KEY: {
    INIT: "INIT",
    ROUTER_EXEC: "ROUTE_EXEC",
  },
};
