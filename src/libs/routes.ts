import { appState, constants } from "../libs";

const { APP_STATES_KEY } = constants;

type Routes = Record<string, string>;

const routes: Routes = {
  "/": "../index.html",
  "/categories": "../categories/categories.html",
};

async function onRouteChange(cb: () => Promise<any>) {
  const count = appState.getState(APP_STATES_KEY.ROUTER_EXEC) ?? 0;
  appState.setState(APP_STATES_KEY.ROUTER_EXEC, count + 1);

  const path = window.location.pathname;
  const contentRoute = routes[path];

  const app = document.getElementById("app");

  if (!contentRoute) {
    app!.innerHTML = "<h1>Error 404 Page not found</h1>";
  } else {
    fetch(contentRoute)
      .then((response) => response.text())
      .then((content) => (app!.innerHTML = content))
      .then(async () => await cb());
  }

  console.info(
    "::: ROUTER EXEC: ",
    appState.getState(APP_STATES_KEY.ROUTER_EXEC)
  );
}

export const initRouter = (cb: () => Promise<any>) => {
  document.addEventListener("DOMContentLoaded", async () => {
    if (!appState.getState(APP_STATES_KEY.INIT)) {
      await onRouteChange(cb);
      window.addEventListener("popstate", () => onRouteChange(cb));
    }
  });
};

// const routes: Record<string, string> = {
//   "/": "../index.html",
//   "/categories": "../categories/categories.html",
// };

// async function onRouteChange() {
//   const path = window.location.pathname;
//   const page = routes[path];

//   const app = document.getElementById("app");

//   if (!page) {
//     app!.innerHTML = "<h1>Error 404 Page not found</h1>";
//   } else {
// await fetch(page)
//   .then((response) => response.text())
//   .then((content) => (app!.innerHTML = content));
//   }
// }

// export const initRouter = (fn: () => Promise<any>) => {
//   document.addEventListener("DOMContentLoaded", async () => {
//     await onRouteChange();
//     window.addEventListener("popstate", onRouteChange);

//     return fn();
//   });
// };
