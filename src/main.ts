import { initRouter } from "./libs/routes";
import { appState, constants } from "./libs";

const { APP_STATES_KEY } = constants;

initRouter(async () => {
  appState.setState(APP_STATES_KEY.INIT, true);

  console.info("HOLA desde INDEX");
});
